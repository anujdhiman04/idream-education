package com.anujkumar.idreameducation;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Home extends AppCompatActivity {

    private static final String TAG = Home.class.getSimpleName();
    DatabaseReference mDB;
    DatabaseReference mListItemRef;
    private RecyclerView rvClassContent;
    private DataAdapter mAdapter;
    private ArrayList<ClassDataItem> myClassDataItems;

    String classData = "Class 1";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        classData = getIntent().getStringExtra("class");

        mDB= FirebaseDatabase.getInstance().getReference();

        mListItemRef = mDB.child(classData);

        myClassDataItems = new ArrayList<>();
        rvClassContent = (RecyclerView)findViewById(R.id.rvClassContent);
        rvClassContent.addItemDecoration(new SimpleDividerItemDecoration(getResources()));
        rvClassContent.setLayoutManager(new LinearLayoutManager(this));
        updateUI();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createNewListItem();
            }
        });

        mListItemRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG+"Added",dataSnapshot.getValue(ClassDataItem.class).toString());
                fetchData(dataSnapshot);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG+"Changed",dataSnapshot.getValue(ClassDataItem.class).toString());


            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

                Log.d(TAG+"Removed",dataSnapshot.getValue(ClassDataItem.class).toString());

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG+"Moved",dataSnapshot.getValue(ClassDataItem.class).toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG+"Cancelled",databaseError.toString());
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch(id){
            case R.id.action_delete_all:
                deleteAllListItems();
                break;
            case R.id.action_logout:
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(this, LoginActivity.class));
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    public void createNewListItem() {
        // Create new List Item  at /classData

        final String key = FirebaseDatabase.getInstance().getReference().child(classData).push().getKey();
        LayoutInflater li = LayoutInflater.from(this);
        View getListItemView = li.inflate(R.layout.dialog_get_list_item, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        alertDialogBuilder.setView(getListItemView);

        final EditText etTitle = (EditText) getListItemView.findViewById(R.id.etTitle);

        final EditText etYoutubeCode = (EditText) getListItemView.findViewById(R.id.etYoutubeCode);
        
        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // get user input and set it to result
                        // edit text
                        String Title = etTitle.getText().toString();
                        String YoutubeCOde = etYoutubeCode.getText().toString();
                        ClassDataItem classDataItem = new ClassDataItem(Title, YoutubeCOde);
                        Map<String, Object> listItemValues = classDataItem.toMap();
                        Map<String, Object> childUpdates = new HashMap<>();
                        childUpdates.put("/"+classData+"/" + key, listItemValues);
                        FirebaseDatabase.getInstance().getReference().updateChildren(childUpdates);

                    }
                }).create()
                .show();

    }

    public void deleteAllListItems(){
        FirebaseDatabase.getInstance().getReference().child(classData).removeValue();
        myClassDataItems.clear();
        mAdapter.notifyDataSetChanged();
        Toast.makeText(this,"Items Deleted Successfully",Toast.LENGTH_SHORT).show();

    }

    private void fetchData(DataSnapshot dataSnapshot) {
        ClassDataItem classDataItem =dataSnapshot.getValue(ClassDataItem.class);
        myClassDataItems.add(classDataItem);
        updateUI();
    }


    private void updateUI(){
        mAdapter = new DataAdapter(this,myClassDataItems);
        rvClassContent.setAdapter(mAdapter);
    }



    public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
        private ArrayList<ClassDataItem> list;
        private Context context;

        public DataAdapter(Context context,ArrayList<ClassDataItem> list) {
            this.context = context;
            this.list = list;

        }

        @Override
        public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.class_content_itemview, viewGroup, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, int i) {

            ClassDataItem classDataItem = list.get(i);

            String text = String.format(context.getResources().getString(R.string.thumbnail_url), classDataItem.getVideoCode());

            Log.d(TAG, "IMAGE THUMBNAIL ==== " + text);

            viewHolder.tvTitle.setText(classDataItem.getTitle());
            viewHolder.tvCreatedDate.setText(classDataItem.getListItemCreationDate());


            Picasso.with(context).load(text).resize(100, 50).into(viewHolder.ivThumbnail);
            
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder{

            public TextView tvTitle,tvCreatedDate;
            public ImageView ivThumbnail;

            public ViewHolder(View view) {
                super(view);

                tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
                tvCreatedDate = itemView.findViewById(R.id.tvCreatedDate);
                ivThumbnail = itemView.findViewById(R.id.ivThumbnail);
            }
        }
    }

}
