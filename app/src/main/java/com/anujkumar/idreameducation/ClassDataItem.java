package com.anujkumar.idreameducation;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class ClassDataItem {

    public String title;
    public String videoCode;
    public String listItemCreationDate;

    public ClassDataItem(String title, String videoCode) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        this.listItemCreationDate = sdf.format(new Date());
        this.title = title;
        this.videoCode = videoCode;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVideoCode() {
        return videoCode;
    }

    public void setVideoCode(String videoCode) {
        this.videoCode = videoCode;
    }

    @Override
    public String toString() {
        return "ClassDataItem{" +
                "title='" + title + '\'' +
                ", videoCode='" + videoCode + '\'' +
                ", listItemCreationDate='" + listItemCreationDate + '\'' +
                '}';
    }


    public void setListItemCreationDate(String listItemCreationDate) {
        this.listItemCreationDate = listItemCreationDate;
    }

    public String getListItemCreationDate() {
        return listItemCreationDate;
    }

    public ClassDataItem() {
        // Default constructor required for calls to DataSnapshot.getValue(ClassDataItem.class)
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("title", title);
        result.put("videoCode", videoCode);
        result.put("listItemCreationDate", listItemCreationDate);
        return result;
    }

}