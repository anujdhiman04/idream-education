package com.anujkumar.idreameducation;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

public class ChooseClass extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    Spinner spClassSelection;
    Button btSelect;

    String[] classes = { "Class 1", "Class 2", "Class 3", "Class 4", "Class 5"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_class);

        spClassSelection = findViewById(R.id.spClassSelection);

        btSelect = findViewById(R.id.btSelect);

        btSelect.setOnClickListener(this);

        setSpinner();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id){
            case R.id.btSelect:
                //Continue
                Intent intent = new Intent(this, Home.class);
                intent.putExtra("class",spClassSelection.getSelectedItem().toString());
                startActivity(intent);
                break;
        }
    }

    public void setSpinner(){
        //Getting the instance of Spinner and applying OnItemSelectedListener on it
        spClassSelection.setOnItemSelectedListener(this);

        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,classes);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        spClassSelection.setAdapter(aa);

        spClassSelection.setSelection(0);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
